const express = require('express');
const spellchecker = require('spellchecker_ptbr');
const request = require('sync-request');
const cheerio = require('cheerio');
const util = require('util');
const app = express();

var banco_de_contracoes = {
  pq: "por que",
  vc: "voce",
  qnd: "quando",
  pfvr: "por favor",
  pf: "por favor",
  ta: "esta",
  msm: "mesmo",
  tb: "tambem",
  tbm:"tambem"

}

var categories = {
  tutorial: ['como', 'tutorial', 'fazer'],
  matematica: ['calcular', 'soma', 'subtracao', 'multiplicacao'],
  historia: ['quando', 'guerra', 'independencia', 'quem', 'foi'],
  programacao: ['programar', 'codigo', 'algoritmo']
}

var plataformas = {
  google: {
      categorias: null,
      searchfun: (txt) => {
        let gogl = request('GET', `https://www.google.com.br/search?q=${txt.join(' ')}`);
        var $ = cheerio.load(gogl.getBody('utf-8'));

        let results = [];
        $('.g a').each((i, obj) => {
          if(i != 0 && $(obj).attr('href').indexOf('webcache') == -1 && $(obj).text() != 'Similares'){
            results.push({
              name: $(obj).text(),
              link: $(obj).attr('href').split('&')[0].replace('/url?q=', '')
            });
          }
        });

        return {type: 'list', results};
      }
  },
  wikipedia: {
      categorias: ['historia', 'matematica'],
      searchfun: (txt) => {

        let searchPage = request('GET', `https://www.google.com.br/search?q=${txt.join(' ')} site:pt.wikipedia.org`);
        var $ = cheerio.load(searchPage.getBody('utf-8'));

        let wikiPage = $('.g a').first().attr('href').split('&')[0].replace('/url?q=', '')
        try {
          wikiPage = request('GET', wikiPage);
          $ = cheerio.load(wikiPage.getBody('utf-8'));

          return {
            type: 'text',
            text: $('p').first().text()
          }
        } catch (e) {
          return {type:'null'};
        }
      }
  },
  youtube: {
    categorias: ['matematica', 'tutorial', 'programacao'],
    searchfun: (txt) => {
      let yt = request('GET', `https://www.youtube.com.br/search?q=${txt.join(' ')}`);
      var $ = cheerio.load(yt.getBody('utf-8'));

      let vid = $('.yt-lockup-title a').first();

      let result = {
        type: 'video',
        title: $(vid).text(),
        link: `http://youtube.com.br/${$(vid).attr('href')}`
      };

      return result;
    }
  }
}

//set up site connections
app.use(express.static('site'));
app.get('/como/:question', function(req, res){
  let txt = req.params.question;

  let chav = Object.keys(banco_de_contracoes);
  for (let i = 0; i < chav.length; i++) {
    let contracao = chav[i];
    txt = txt.split(contracao).join(banco_de_contracoes[contracao]);
  }

  spellchecker.spellcheck(txt, (err,typos) => {
    if (err) console.error("Ocorreu um erro " + err);
    for (let i = 0; i < typos.length; i++) {
      txt = txt.split(typos[i].word).join(typos[i].suggestions[0]);
    }

    txt = txt.split(/\s/g);

    let meaning = {};
    let importantWords = 0;
    for (let i = 0; i < txt.length; i++) {
      let cat = Object.keys(categories);
      let important = false;
      for (let j = 0; j < cat.length; j++) {
        if(categories[cat[j]].includes(txt[i])){
          if(meaning[cat[j]] == undefined) meaning[cat[j]] = 0;
          meaning[cat[j]]++;
          important = true;
        }
      }

      if(important){
        importantWords++;
      }
    }

    let mean = Object.keys(meaning);
    for (let i = 0; i < mean.length; i++) {
        meaning[mean[i]] /= importantWords;
    }

    let results = [];
    for (var eng in plataformas) {
      if (plataformas.hasOwnProperty(eng)) {
        if(plataformas[eng].categorias == null)
          results.push(plataformas[eng].searchfun(txt));
        else{
          let relevant = false;
          let mean = Object.keys(meaning);
          for (let i = 0; i < mean.length; i++) {
              if(meaning[mean[i]] >= 0.5 && plataformas[eng].categorias.includes(mean[i])) relevant = true;
          }

          if(relevant)
            results.push(plataformas[eng].searchfun(txt));
        }

      }
    }

    var text = '';
    let foundText = false;
    for (var i = 0; i < results.length; i++) {
      if(results[i].type == 'text' && !foundText){
        text = results[i].text + '\n' + text;
      } else if(results[i].type == 'video'){
        text += `\nOu que tal tentar esse video? ${results[i].title} - ${results[i].link}\n`
      } else if(results[i].type == 'list'){
        text += '\nEncontrei estes links:\n'
        for(let j = 0; j < 5; j++){
          if(results[i].results[j]){
            text += `${results[i].results[j].name} - ${results[i].results[j].link}\n`
          }
        }
      }
    }

    //res.send(JSON.stringify({txt, meaning, results}));
    res.send(text)
  });
});

app.listen(8080, () => {
  console.log('Servidor iniciado na porta: 8080');
});
